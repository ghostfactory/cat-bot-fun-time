var http = require('http-request');
var fs = require('fs');

var datestring = function() {
    var d = new Date(Date.now() - 5 * 60 * 60 * 1000); //est timezone
    return d.getUTCFullYear() + "-" +
    (d.getUTCMonth() + 1) + "-" +
    d.getDate();
}

var handleError = function(err) {
    console.log("\n\n" + timeStamp() + "ERROR:");
    console.error(timeStamp() + "Response status:", err.statusCode);
    console.error(timeStamp() + "Data:", err.data);
}

var randIndex = function(arr) {
    var index = Math.floor(arr.length * Math.random());
    return arr[index];
};

var download = function (url, filename, callback) {
    var options = {url: url};
    http.get(options, filename, function (error, result) {
        if (error) {
            console.error(timeStamp() + 'Error downloading file: ' + error);
            return callback(error);
        } else {
            console.log(timeStamp() + 'File downloaded: ' + result.file);
            callback();
        }
    });
};

var makeID = function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 15; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

var timeStamp = function() {
    var d = new Date();
    var year = d.getFullYear();
    var month = ( (d.getMonth() + 1).toString().length == 1 ? '0' + (d.getMonth() + 1) : (d.getMonth() + 1) );
    var day = ( d.getDate().toString().length == 1 ? '0' + d.getDate() : d.getDate() );
    var hours = ( d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours() );
    var minutes = ( d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes() );
    var seconds = ( d.getSeconds().toString().length == 1 ? '0' + d.getSeconds() : d.getSeconds() );
    return "(" + year + "/" + month + "/" + day + " " + hours + ":" + minutes + ":" + seconds + ") ";
}

/* ================================================== */
/* Exports
/* ================================================== */

module.exports = {
    datestring: datestring,
    timeStamp: timeStamp,
    handleError: handleError,
    randIndex: randIndex,
    download: download,
    makeID: makeID
};