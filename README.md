Cat Bot Fun Time
=====================
Super sweet Twitter bot that posts pictures of cats and that's about it. Neato! See it in action on [Twitter](https://twitter.com/CatBotFunTime).

Install
---------------------
Run `sudo npm install`.

Configuration
---------------------
1. To configure that bot, you must have a developer account with Twitter. 
2. Create your application in the Twitter Application Management, and generate your Consumer Keys and Access Tokens.
3. Create a config file at `config/config.js`
4. Add the following configuration:
```javascript
module.exports = {
    consumer_key: YOUR_CONSUMER_KEY, 
    consumer_secret: YOUR_CONSUMER_SECRET, 
    access_token: YOUR_ACCESS_TOKEN, 
    access_token_secret: YOUR_ACCESS_TOKEN_SECRET
}

tuwmConfig = {
    consumer_key: module.exports.consumer_key,
    consumer_secret: module.exports.consumer_secret,
    token: module.exports.access_token,
    token_secret: module.exports.access_token_secret
};
```