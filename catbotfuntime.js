var config = require("./config/config");
var util = require("./lib/util");
var fs = require('fs');

var Bot = require("./bot");
var CatBot = new Bot(config);
var Entities = require('html-entities').XmlEntities;

var entities = new Entities();

var options = {
    searchTopic: '#cat',
    subreddit: 'cats',
    memoryLength: 30,
};

var timers = {
    // Prod
    targetFollow: 1 * 60 * 60000,
    mingle: 2 * 60 * 60000,
    tweetPhoto: 2 * 60 * 60000,
    favorite: 1 * 60 * 60000,
    prune: 5 * 60 * 60000,

    //// Test
    //targetFollow: 3 * 60000,
    //mingle: 4 * 60000,
    // tweetPhoto: 1 * 30000,
    //favorite: 1.5 * 60000,
    //prune: 6 * 60000
};

var strings = {
    initiate: "Cat Bot Fun Time initiated!",
    action: "Cat Bot decides to "
};


/* ====================================================== */
/* Run!! Initiate!! Go!!
/* ====================================================== */
(function() {

    process.title = "catbotfuntime";

    console.log( strings.initiate );

    setInterval(targetFollow, timers.targetFollow);
    setInterval(mingle, timers.mingle);
    setInterval(tweetPhoto, timers.tweetPhoto);
    setInterval(favorite, timers.favorite);
    setInterval(prune, timers.prune);

})();


/* ====================================================== */
/* Target Follow
/* ====================================================== */
function targetFollow()
{
    var action = '[Target Follow] ';
    console.log(util.timeStamp() + strings.action + action);

    var params = {
        q: options.searchTopic,
        since: util.datestring(),
        result_type: "mixed",
        lang: "us"
    };

    CatBot.searchFollow(params, function(err, reply) {
        if(err) return util.handleError(err);

        var name = reply.screen_name;
        console.log(util.timeStamp() + action + "Followed @" + name);
    });
}


/* ====================================================== */
/* Mingle
/* ====================================================== */
function mingle()
{
    var action = '[Mingle] ';
    console.log(util.timeStamp() + strings.action + action);

    CatBot.mingle(function(err, reply) {
        if(err) return util.handleError(err);

        var name = reply.screen_name;
        console.log(util.timeStamp() + action + "Followed: @" + name);
    });
}


/* ====================================================== */
/* Tweet Photo
/* ====================================================== */
function tweetPhoto()
{
    var action = '[Tweet Photo] ';
    console.log(util.timeStamp() + strings.action + action);

    CatBot.redditRandomPost(options.subreddit, function(url) {
        url = entities.decode(url);
        console.log(util.timeStamp() + action + "Retrieved image: " + url);

        checkDuplicateURL(url, function(response) {
            if(response === null) {
                console.log(util.timeStamp() + "Problem reading or writing memory, not tweeting photo");
                return;
            }

            if(response === false) {
                console.log(util.timeStamp() + "Photo already tweeted. Trying new photo.");
                tweetPhoto();
                return;
            }

            CatBot.tweetPhotoFromUrl(url);
        });
    });
}


/* ====================================================== */
/* Favorite
/* ====================================================== */
function favorite()
{
    var action = '[Favorite] ';
    console.log(util.timeStamp() + strings.action + action);

    var params = {
        q: options.searchTopic,
        since: util.datestring(),
        result_type: "mixed",
        lang: "us"
    };

    CatBot.favorite(params, function(err, reply) {
        if(err) return util.handleError(err);

        console.log(util.timeStamp() + action + "Favorite response: " + reply.id);
    });
}


/* ====================================================== */
/* Prune
/* ====================================================== */
function prune()
{
    var action = '[Prune] ';
    console.log(util.timeStamp() + strings.action + action);

    CatBot.prune(function(err, reply) {
        if(err) return util.handleError(err);

        var name = reply.screen_name;
        console.log(util.timeStamp() + action + "Unfollowed: @" + name);
    });
}


/* ====================================================== */
/* Helper Functions
/* ====================================================== */

function checkDuplicateURL(url, callback)
{
    var path = './memory.json';
    fs.readFile(path, function(err, data) {

        if(err){
            console.log(util.timeStamp() + "Error reading memory: " + err);
            return callback(null);
        }

        data = JSON.parse(data);
        if (data.indexOf(url) >= 0) {
            console.log(util.timeStamp() + "URL in memory: " + url);
            return callback(false);
        }

        data.unshift(url);

        console.log(util.timeStamp() + "Memory has: " + data.length);
        console.log(util.timeStamp() + "Memory allowed: " + options.memoryLength);
        var memoryDiff = data.length - options.memoryLength;
        if(memoryDiff > 0) {
            var removed = data.splice(-memoryDiff, memoryDiff);
            console.log(util.timeStamp() + "Memory is removing amount: " + memoryDiff);
            console.log(util.timeStamp() + "Removed memories: ");
            console.log(util.timeStamp() + removed);
        }

        data = JSON.stringify(data);

        fs.writeFile(path, data, function(err) {

            if(err){
                console.log(util.timeStamp() + "Error writing memory: " + err);
                return callback(null);
            }

            console.log( util.timeStamp() + "Memory written with new image: " + url );
            return callback(true);

        });

    });
}
