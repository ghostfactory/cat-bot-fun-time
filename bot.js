//
//  Bot
//  class for performing various twitter actions
//
var config = require('./config/config.js');
var util = require('./lib/util.js');
var fs = require('fs');

var Twit = require('./node_modules/twit/lib/twitter');
var TwitterMedia = require('./lib/twitter-media');
var Snoocore = require('./node_modules/snoocore/Snoocore');

var Reddit = new Snoocore({ userAgent: 'CatBotFunTime' });
var Bot = module.exports = function(config) {
    this.twit = new Twit(config);
};
var TUWM = new TwitterMedia(tuwmConfig);


//
//  get a random post url from reddit
//
Bot.prototype.redditRandomPost = function(subreddit, callback) {
  Reddit('/r/$subreddit/hot').listing({
    $subreddit: subreddit,
    limit: 50
  }).then(function(slice) {
    var post = util.randIndex(slice.children);

    if(!post) {
        console.log("Undefined post... Not getting random reddit post.");
        return;
    }

    var url = post.data.url;

    callback(url);
  });
}


//
//  tweet a photo from a url
//
Bot.prototype.tweetPhotoFromUrl = function(url) {
    var path = './temp/' + util.makeID() + '.jpg';

    util.download(url, path, function(err){
        if(err) {
            console.log(util.timeStamp() + "Error downloading image, not tweeting photo.");
            return;
        }

        TUWM.post('', path, function(err, response) {
            if (err) {
                console.log(util.timeStamp() + "Error posting image: " + err);
                return null;
            }
            console.log(util.timeStamp() + "Image tweeted: " + path);

            fs.unlink(path, function(err) {
                if (err) {
                    console.log(util.timeStamp() + "Error deleting image: " + err);
                    return null;
                }
                console.log(util.timeStamp() + "Image deleted: " + path);
            });
        });
    });
}


//
//  post a tweet
//
Bot.prototype.tweet = function (status, callback) {
  if(typeof status !== 'string') {
    return callback(new Error(util.timeStamp() + 'Tweet must be of type String'));
  } else if(status.length > 140) {
    return callback(new Error(util.timeStamp() + 'Tweet is too long: ' + status.length));
  }
  this.twit.post('statuses/update', { status: status }, callback);
};


//
// retweet a random tweet based on search
//
Bot.prototype.retweet = function (params, callback) {
  var self = this;

  self.twit.get('search/tweets', params, function (err, reply) {
    if(err) return callback(err);

    var tweets = reply.statuses;
    var randomTweet = util.randIndex(tweets);

    if(!randomTweet) {
        console.log("Undefined random tweet... Not retweeting random tweet.");
        return;
    }

    self.twit.post('statuses/retweet/:id', { id: randomTweet.id_str }, callback);
  });
};


//
// favorite a random tweet based on search
//
Bot.prototype.favorite = function (params, callback) {
  var self = this;

  self.twit.get('search/tweets', params, function (err, reply) {
    if(err) return callback(err);

    var tweets = reply.statuses;
    var randomTweet = util.randIndex(tweets);

    if(!randomTweet) {
        console.log("Undefined random tweet... Not favoriting random tweet.");
        return;
    }

    self.twit.post('favorites/create', { id: randomTweet.id_str }, callback);
  });
};


//
//  follow a random user based on a search
//
Bot.prototype.searchFollow = function (params, callback) {
  var self = this;

  self.twit.get('search/tweets', params, function (err, reply) {
    if(err) return callback(err);

    var tweets = reply.statuses;

    var randomTweet = util.randIndex(tweets);

    if(!randomTweet) {
        console.log("Undefined random tweet... Not following random user.");
        return;
    }

    var target = randomTweet.user.id_str;

    self.twit.post('friendships/create', { id: target }, callback);
  });
};


//
//  choose a random friend of one of your followers, and follow that user
//
Bot.prototype.mingle = function (callback) {
  var self = this;

  this.twit.get('followers/ids', function(err, reply) {
      if(err) { return callback(err); }

      var followers = reply.ids
        , randFollower  = util.randIndex(followers);

      self.twit.get('friends/ids', { user_id: randFollower }, function(err, reply) {
          if(err) { return callback(err); }

          var friends = reply.ids
            , target  = util.randIndex(friends);

          self.twit.post('friendships/create', { id: target }, callback);
        })
    })
};


//
//  prune your followers list; unfollow a friend that hasn't followed you back
//
Bot.prototype.prune = function (callback) {
  var self = this;

  this.twit.get('followers/ids', function(err, reply) {
      if(err) return callback(err);

      var followers = reply.ids;

      self.twit.get('friends/ids', function(err, reply) {
          if(err) return callback(err);

          var friends = reply.ids
            , pruned = false;

          while(!pruned) {
            var target = util.randIndex(friends);

            if(!~followers.indexOf(target)) {
              pruned = true;
              self.twit.post('friendships/destroy', { id: target }, callback);
            }
          }
      });
  });
};
